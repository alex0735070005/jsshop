import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import { BrowserRouter } from 'react-router-dom';


// 1) Импорт провайдера из react-redux
import {Provider} from 'react-redux';

// Метод для добавления dev tools
import { composeWithDevTools } from 'redux-devtools-extension';

// 2) Импорт хранилища из redux
import { createStore, applyMiddleware } from 'redux';

// 3) Подключаем главный reduser
import reducer from './reducers';

// Промежуточный слой для написания асинхронных запросов для redux
import thunk from 'redux-thunk';

// 4) Создаем объект хранилища, передаем туда наш reduser и подключаем инструменты разработки redux
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App /> 
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
