import React, { Component } from 'react';
import {connect} from 'react-redux';
import {addUserAction} from '../actions/app';

class Registration extends Component {

    change = (e)=> {

        let data = {};

        data[e.target.name] = e.target.value;

        this.props.changeUserData(data);
    }

    add = ()=> {

        let data = {};

        data['email'] = this.props.data.email;
        data['password'] = this.props.data.password;
       
        this.props.addUser(data);
    }


    render() {
      
      return (
        <div className="registration page">
            <h3>Registration</h3>
            <form>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input onInput={this.change} name="email" type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input onInput={this.change}  name="password" type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                </div>
                <button onClick = {this.add} type="button" className="btn btn-primary">Submit</button>
            </form>
        </div>
      );

  }


}


export default connect(
    state => ({
        data:state.registration
    }),
    dispatch => ({
        changeUserData:(d) => {            
            dispatch({type:'CHANGE_DATA_USER', data:d});
        },
        addUser:(data) => {            
            dispatch(addUserAction(data));
        },
    })
)(Registration);