import {combineReducers} from 'redux';


import cart from './cart';
import app from './app';
import registration from './registration';


export default combineReducers({
    cart,
    app,
    registration
});