
const initState = {
    login:'',
    email:'',
    password:'',
    messageSuccess:0,
    messageError:0
};


export default function app(state = initState, action)
{
    switch(action.type)
    {
        case 'CHANGE_DATA_USER':

        return {
                ...state,
                ...action.data
            };

        case 'ADD_SUCCESS_USER':

        return {
                ...state,
                ...initState
            };

        case 'SHOW_SUCCESS_USER':

        return {
                ...state,
                messageSuccess:1
            };

        case 'HIDE_SUCCESS_USER':

        return {
                ...state,
                messageSuccess:0
            };

        default: return state;     
    }
}